/**
 * This is a sample code for a PT6955 7 segment LCT driver
 *
 * from the data sheet:
 * PT6955 is an LED Controller driven on a 1/4 to 1/7 duty factor. Twelve/nine segment output lines, 4 to
 * 7 grid output lines, one display memory, control circuit are all incorporated into a single chip to build a
 * highly reliable peripheral device for a single chip microcomputer. Serial data is fed to PT6955 via a
 * three-line serial interface. Housed in a 24-pin SOP, PT6955’s pin assignments and application circuit
 * are optimized for easy PCB Layout and cost saving advantages. 
 */
 
/** define som commandes */
#define DISPLAYMODE     0X02        //bit7,bit6=00:CMD1 
                                    //bit1,bit0=00: 4Grids,12Segments 
                                    //bit1,bit0=01: 5Grids,11Segments 
                                    //bit1,bit0=10: 6Grids,10Segments 
                                    //bit1,bit0=11: 7Grids,9Segments 
                                     
#define DATASET         0X40        //bit7,bit6=01:CMD2 
                                    //bit3=1: TEST MODE ;bit3=0: NORMAL MODE 
                                    //bit2=1: FIXES ADDR;bit2=0: INCREMENT ADDR 
                                    //bit1,bit0=00: Write Data to Display Mode 
                                     
#define ADDRSET         0XC0        //bit7,bit6=11:CMD3 
                                    //bit3-bit0=0000: DATA ADDR 
 
#define DISPLAYCONTROL  0X8C        //bit7,bit6=10:CMD4 
                                    //bit3=1: Display ON ;bit3=0: Display OFF 
                                    //bit2-bit0=01:100 Pulse width = 11/16 
 
/**
 * Bit masks for the number 0..9 of a 7 segment LED character
 *
 *
 *      --bit0--
 *      |      |
 *      |      |
 *    bit6    bit1
 *      |      |
 *      |      |
 *      --bit6--
 *      |      |
 *      |      |
 *    bit4    bit2
 *      |      |
 *      |      |
 *      --bit4--    o bit7
 *
 */
#define MASK_EMPTY 0x00
#define MASK_0 0x3F
#define MASK_1 0x06
#define MASK_2 0x5B
#define MASK_3 0x4F
#define MASK_4 0x66
#define MASK_5 0x6D
#define MASK_6 0x7D
#define MASK_7 0x07
#define MASK_8 0x7F
#define MASK_9 0x6F
#define MASK_DOT 0x80

 /** global vars */
static int pin_data = 2;
static int pin_clock = 3;
static int pin_cs = 4;

/**
 * Write a byte to the SPI 
 */   
static void SPIOut(unsigned char data)    
{       
    unsigned char i;   
       
    for(i=0;i<8;i++)   
    {   
        digitalWrite(pin_clock, LOW);           
        if(data&0X01)   
        {   
            digitalWrite(pin_data, HIGH);   
        }   
        else   
        {   
            digitalWrite(pin_data, LOW);
        }   
        data>>=1;   
        digitalWrite(pin_clock, HIGH);
    }   
}   

/**
 * write a command (a single byte) to SPI
 */
void SPICmd(byte data)
{
  digitalWrite(pin_cs, LOW);
  SPIOut(data); 
  digitalWrite(pin_cs, HIGH);
}


/**
 * switch display off
 *
 *  The content of the is store in the PT6955. 
 */
void setDisplayOff()
{
  byte DisplayContrl_OFF = 0X84;        //bit7,bit6=10:CMD4 
  SPICmd(DisplayContrl_OFF);
}

/**
 * switch on the display
 * and show the content sored in the memory.
 */
void setDisplayOn()
{
  byte DisplayContrl_ON = 0X8C;        //bit7,bit6=10:CMD4 
                                      //bit3=1: Display ON ;bit3=0: Display OFF 
                                      //bit2-bit0=01:100 Pulse width = 11/16 
  SPICmd(DisplayContrl_ON);
}

/**
 * show the content of a 4 character 7 segment display.
 */
static void SPIShowRaw(byte d1, byte d2, byte d3, byte d4)
{
  byte displayBuf[4] = { d1, d2, d3, d4};
  
  SPICmd(DATASET);
  
  digitalWrite(pin_cs, LOW);
  SPIOut(ADDRSET);        //·¢ËÍµØÖ·ÉèÖÃ
  int i;  
  for(i=0;i<sizeof(displayBuf);i++)   
  {   
    SPIOut(displayBuf[i]);   
    SPIOut(0);   
  }   
  SPIOut(DISPLAYMODE); 
  digitalWrite(pin_cs, HIGH);
}



/**
 * @short init function of the PT6955 7 segment display driver
 *
 * Assign the pins used for the communication with the PT6955.
 *
 * @param pinData pin # of the pin used for the data pin #
 * @param pinClock clock pin #
 * @param pin_cs chip select pin #
 */
void s_init(int pinData, int pinClock, int pinCs)
{
  pin_data = pinData;
  pin_clock = pinClock;
  pin_cs = pinCs;


  /* setup pin modes */
  pinMode(pin_data, OUTPUT);
  pinMode(pin_clock, OUTPUT);
  pinMode(pin_cs, OUTPUT);

  SPICmd(DISPLAYMODE);
}

/**
 * initialize the VFD display
 */
void setup() 
{  
   // Assign the data, clock and chipselect pins
   s_init(7, 6, 5);
}

/**
 * Endless loop, just to show how it works...
 */
void loop() 
{ 
    SPIShowRaw(MASK_1, MASK_2, MASK_3, MASK_4 );
    delay(1000);  
        
    SPIShowRaw(MASK_5, MASK_6, MASK_7, MASK_8 );
    delay(1000);  
    
    SPIShowRaw(MASK_9, MASK_0, MASK_DOT, MASK_DOT );
    delay(1000);  

    int i;
    for(int i=0; i<5; i++)
    {
      setDisplayOff();
      delay(300);
      setDisplayOn();
      delay(300);
    }
}

